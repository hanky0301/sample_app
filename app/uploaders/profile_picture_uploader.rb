# encoding: utf-8

class ProfilePictureUploader < CarrierWave::Uploader::Base
	include Cloudinary::CarrierWave

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
	
	process convert: 'png'
	process tags: ['post_picture']

	version :standard do
#		process eager: true
		resize_to_limit(150, 150)
	end

	version :thumbnail do
	#	resize_to_fit(80, 50)
	end
	
	version :small do
		resize_to_fit(30, 30)
	end

  def default_url(*args)
		ActionController::Base.helpers.asset_path([version_name, "avatar.jpg"].compact.join('_'))
  end
  
  # Add a white list of extensions which are allowed to be uploaded.
  def extension_white_list
    %w(jpg jpeg gif png)
  end

	# storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:

  # Provide a default URL as a default if there hasn't been a file uploaded:

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end

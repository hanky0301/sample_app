class ApplicationMailer < ActionMailer::Base
  default from: "noreply@vid-conf.herokuapp.com"
  layout 'mailer'
end

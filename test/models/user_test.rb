require 'test_helper'

class UserTest < ActiveSupport::TestCase
	
	def setup
		@user = User.new(name: "Example User", email: "user@example.com", 
										 password: "passwd", password_confirmation: "passwd")
	end

	test "should be valid" do
		assert @user.valid?
	end

	test "name should be present" do
		@user.name = "   "
		assert_not @user.valid?
	end

	test "email should be present" do
		@user.email = "   "
		assert_not @user.valid?
	end

	test "name should not be too long" do
		@user.name = 'a' * 51
		assert_not @user.valid?
	end

	test "email should not be too long" do
		@user.email = 'a' * 244 + "@example.com"
		assert_not @user.valid?
	end

	test "email validation should accept valid addresses" do
		valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org 
												 first.last@foo.jp alice+bob@baz.cn]
		valid_addresses.each do |addr|
			@user.email = addr
			assert @user.valid?, "#{addr.inspect} should be valid"
		end
	end

	test "email validation should reject invalid addresses" do
		invalid_address = %w[user@example,com user_at_foo.org user.name@example.
												 foo@bar_baz.com foo@bar+baz.com]
		invalid_address.each do |addr|
			@user.email = addr
			assert_not @user.valid?, "#{addr.inspect} should be invalid"
		end
	end

	test "email addresses should be unique" do
		dup_user = @user.dup
		dup_user.email = @user.email.upcase
		@user.save
		assert_not dup_user.valid?
	end

	test "password should be present" do
		@user.password = @user.password_confirmation = " " * 6
		assert_not @user.valid?
	end

	test "password should not be too short" do
		@user.password = @user.password_confirmation = "a" * 5
		assert_not @user.valid?
	end

	test "authenticated? should return false for a user with nil digest" do
		assert_not @user.authenticated?(:remember, '')
	end

	test "associated microposts should be destroyed" do
		@user.save
		@user.microposts.create!(content: "Hi there")
		assert_difference 'Micropost.count', -1 do
			@user.destroy
		end
	end

	test "should follow and unfollow a user" do
		test_user = users(:test_user)
		archer = users(:archer)
		assert_not test_user.following?(archer)
		test_user.follow(archer)
		assert test_user.following?(archer)
		assert archer.followers.include?(test_user)
		test_user.unfollow(archer)
		assert_not test_user.following?(archer)
	end

	test "should have the right posts" do
		test_user = users(:test_user)
		archer = users(:archer)
		lana = users(:lana)

		lana.microposts.each do |post_following|
			assert test_user.feed.include?(post_following)
		end

		archer.microposts.each do |post_unfollowed|
			assert_not test_user.feed.include?(post_unfollowed)
		end
	end
end
